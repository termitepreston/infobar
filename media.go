package main

import (
	"fmt"
	"time"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
)

var dest = `org.mpris.MediaPlayer2.mpv`
var objPath dbus.ObjectPath = `/org/mpris/MediaPlayer2`

var propInterface = `org.freedesktop.DBus.Properties.Get`
var appInterface = `org.mpris.MediaPlayer2`
var playerInterface = `org.mpris.MediaPlayer2.Player`

var prevCmd = `dbus-send --session --type=method_call --dest=org.mpris.MediaPlayer2.mpv /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous`
var nextCmd = `dbus-send --session --type=method_call --dest=org.mpris.MediaPlayer2.mpv /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next`
var paplCmd = `dbus-send --session --type=method_call --dest=org.mpris.MediaPlayer2.mpv /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause`

var mpvTemplate = `%%{A1:%s:}%s%%{A} %%{A1:%s:}%s%%{A} %%{A1:%s:}%s%%{A} [%s]`

func MpvApplet(dt time.Duration, ch chan<- Msg) {
	for {
		// try to call some function on mpv
		// if there is error
		// 	print empty string
		// else
		// 	...

		conn, err := dbus.SessionBus()
		if err != nil {
			ch <- Msg{
				Mpv,
				fmt.Sprintf("%s", err.Error()),
			}
			time.Sleep(dt)
			continue
		}

		obj := conn.Object(dest, objPath)
		call := obj.Call(propInterface, 0,
			appInterface, "Identity")

		// We do not have mpv running
		if call.Err != nil {
			ch <- Msg{Mpv, ""}
			time.Sleep(dt)
			continue
		}

		// if status == playing
		//	iconPaPl = iconPa
		// else
		//	iconPaPl = iconPl

		status, err := playerStatus(obj.(*dbus.Object))

		if err != nil {
			ch <- Msg{Mpv, ""}
			time.Sleep(dt)
			continue
		}

		iconPlayOrPause := ""
		if status == "Playing" {
			iconPlayOrPause = iconPause
		} else {
			iconPlayOrPause = iconPlay
		}

		// Get time
		pos, err := playerPosition(obj.(*dbus.Object))
		if err != nil {
			ch <- Msg{Mpv, ""}
			continue
		}

		// we have mpv running
		out := fmt.Sprintf(mpvTemplate,
			prevCmd,
			iconPrevious,
			paplCmd,
			iconPlayOrPause,
			nextCmd,
			iconNext,
			pos)

		ch <- Msg{Mpv, out}

		time.Sleep(dt)
	}
}

func playerStatus(obj *dbus.Object) (string, error) {
	call := obj.Call(propInterface, 0, playerInterface, "PlaybackStatus")
	if call.Err != nil {
		return "", errors.Wrap(call.Err, "error:")
	}

	// call.Body == []interface{} == []dubs.Variant

	return call.Body[0].(dbus.Variant).Value().(string), nil
}

func playerPosition(obj *dbus.Object) (string, error) {
	call := obj.Call(propInterface, 0, playerInterface, "Position")
	if call.Err != nil {
		return "", errors.Wrap(call.Err, "error:")
	}

	uS := call.Body[0].(dbus.Variant).Value().(int64)

	pos := time.Duration(uS) * time.Microsecond

	return formatDuration(pos), nil
}

func formatDuration(t time.Duration) string {
	if t.Hours() >= 1 {
		rt := t.Round(1 * time.Second)
		h := rt / time.Hour
		rt -= h * time.Hour
		m := rt / time.Minute
		rt -= m * time.Minute
		s := rt / time.Second
		return fmt.Sprintf("%02d:%02d:%02d", h, m, s)
	} else {
		rt := t.Round(1 * time.Second)
		m := rt / time.Minute
		rt -= m * time.Minute
		s := rt / time.Second
		return fmt.Sprintf("%02d:%02d", m, s)
	}
}
package applets

import (
	"fmt"
	"os/exec"
	"strings"
	"syscall"
	"time"

	"github.com/pkg/errors"
)

var volTemplate = "%s%%{A1:pamixer -i 2:}%%{A3:pamixer -d 2:}%s%%{A}%%{A} %%{A:pamixer -t:}%d%%%%{A}%s"

func VolumeApplet(dt time.Duration, ch chan<- Msg) {
	for {
		vol, err := volume()
		if err != nil {
			vol = 0
		}

		out := ""

		if isMute, _ := isMute(); isMute {
			out = fmt.Sprintf(volTemplate, ColorLightRed,
				iconVolumeUp,
				vol, ColorBackground)
		} else {
			out = fmt.Sprintf(volTemplate, ColorBackground,
				iconVolumeUp,
				vol, ColorBackground)
		}

		ch <- Msg{Volume, out}
		time.Sleep(dt)
	}
}

func isMute() (bool, error) {
	cmd := exec.Command("pamixer", "--get-mute")

	var output strings.Builder

	cmd.Stdout = &output
	err := cmd.Run()
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			if ws.ExitStatus() != 1 {
				return false, errors.Wrap(err, "volume error.")
			}
		}
	}

	state := output.String()[0]

	if state == 't' {
		return true, nil
	}

	return false, nil
}

func volume() (uint, error) {
	cmd := exec.Command("pamixer", "--get-volume")
	var output strings.Builder
	var volume uint

	cmd.Stdout = &output
	err := cmd.Run()
	if err != nil {
		// struct embedding
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			if ws.ExitStatus() != 1 {
				return 0, errors.Wrap(err, "volume error")
			}
		}
	}

	n, err := fmt.Sscanf(output.String(), "%d\n", &volume)
	if n != 1 || err != nil {
		return 0, errors.Wrap(err, "vol error scanning.")
	}
	return volume, nil
}
