package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"path/filepath"
	"time"

	"github.com/pkg/errors"
)

type status uint

const (
	statusDischarging status = iota
	statusCharging
	statusFull
)

// Path to power stats.
const (
	powerDir = "/sys/class/power_supply/BAT1"
	critical = 15
)

func PowerApplet(dt time.Duration, ch chan<- Msg) {
	chargeFullFile := filepath.Join(powerDir, "charge_full")
	chargeNowFile := filepath.Join(powerDir, "charge_now")

	for {
		chargeFull, err := chargeLevel(chargeFullFile)
		if err != nil {
			ch <- Msg{Power, "error: reading battery stat..."}
			time.Sleep(dt)
			continue
		}

		chargeNow, err := chargeLevel(chargeNowFile)
		if err != nil {
			ch <- Msg{Power, "error: reading battery stat..."}
			time.Sleep(dt)
			continue
		}

		per := percentage(chargeNow, chargeFull)

		// Battery states:
		// + discharging
		// 	+ normally discharging ( 15 - 99 % )
		//		+ [0-15%)  red and blinking
		//		+ [15-50%)  orange
		//		+ [50-75%)  yellow
		//		+ [75-100%)  green
		// + charging and not full
		//	+ blue
		// + charging and full
		//	+ blue
		status, err := chargingStatus()
		if err != nil {
			ch <- Msg{Power, err.Error()}
			continue
		}

		out := ""
		switch status {
		case statusDischarging:
			if per >= 0 && per < 15 {
				out = fmt.Sprintf("%sBAT: %d%%%s", ColorDarkRed, per, ColorBackground)
			} else if per >= 15 && per < 50 {
				out = fmt.Sprintf("%sBAT: %d%%%s", ColorOrange, per, ColorBackground)
			} else if per >= 50 && per < 75 {
				out = fmt.Sprintf("%sBAT: %d%%%s", ColorYellow, per, ColorBackground)
			} else if per >= 75 && per < 100 {
				out = fmt.Sprintf("%sBAT: %d%%%s", ColorDarkGreen, per, ColorBackground)
			}
		case statusCharging:
			out = fmt.Sprintf("%sBAT(%s): %d%%%s", ColorDarkBlue, iconPowerCord, per, ColorBackground)
		case statusFull:
			out = fmt.Sprintf("%sBAT(%s): %d%%%s", ColorDarkBlue, iconPowerCord, per, ColorBackground)
		}

		ch <- Msg{Power, out}
		time.Sleep(dt)
	}
}

// read in battery status.
func chargingStatus() (status, error) {
	f, err := os.Open(filepath.Join(powerDir, "status"))
	if err != nil {
		return statusDischarging, errors.Wrap(err, "error reading file.")
	}

	var r rune
	fmt.Fscanf(f, "%c\n", &r)

	f.Close()

	switch r {
	case 'D':
		return statusDischarging, nil
	case 'C':
		return statusCharging, nil
	case 'F':
		return statusFull, nil
	}

	return statusDischarging, errors.New("error unknown status.")
}

// Return file content as int.
func chargeLevel(filepath string) (uint, error) {
	var readTo uint
	content, err := ioutil.ReadFile(filepath)
	if err != nil {
		return 0, errors.Wrap(err, "cannot read file.")
	}

	n, err := fmt.Sscanf(string(content), "%d\n", &readTo)
	if err != nil || n != 1 {
		return 0, errors.Wrap(err, "problems scanning string.")
	}

	return readTo, nil
}

func percentage(s, l uint) uint {
	ratio := float64(s) / float64(l) * 100
	return uint(math.Trunc(ratio))
}
