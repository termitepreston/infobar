package main

type Msg struct {
	Id  int
	Str string
}

const (
	Clock = iota
	Date
	Power
	Volume
	Mpv
	Scroll
)

// colors
const (
	ColorDarkGreen  = "%{F#859900}"
	ColorYellow     = "%{F#B58900}"
	ColorOrange     = "%{F#FFB144}"
	ColorDarkRed    = "%{F#DC322F}"
	ColorLightRed   = "%{F#CB4B16}"
	ColorDarkBlue   = "%{F#268BD2}"
	ColorReset      = "%{F-}"
	ColorBackground = "%{F#657b83}"
	ColorForeground = "%{B#FDF6e3}"
)

// icons
const (
	iconPowerCord = "\uf1e6"
	iconVolumeUp  = "\uf028"
	iconPause     = "\uf04c"
	iconPlay      = "\uf04b"
	iconPrevious  = "\uf048"
	iconNext      = "\uf051"
)
