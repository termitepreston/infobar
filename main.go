package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/termitepreston/infobar/applets"
)

func main() {
	ch := make(chan applets.Msg)
	go applets.PowerApplet(200*time.Millisecond, ch)
	go applets.ClockApplet(500*time.Millisecond, ch)
	go applets.DateApplet(2*time.Minute, ch)
	go applets.VolumeApplet(150*time.Millisecond, ch)
	go applets.MpvApplet(150*time.Millisecond, ch)
	format := "%%{S0}%s%s%%{l}┃ %s%%{r}%s ┃ %s ┃ %s ┃ %s ┃ %s ┃\n"

	var clockStr, powerStr, dateStr, volStr, mpvStr string

	for {
		msg := <-ch
		switch msg.Id {
		case applets.Clock:
			clockStr = msg.Str
		case applets.Power:
			powerStr = msg.Str
		case applets.Date:
			dateStr = msg.Str
		case applets.Volume:
			volStr = msg.Str
		case applets.Mpv:
			mpvStr = msg.Str
		}
		fmt.Fprintf(os.Stdout,
			format,
			applets.ColorBackground,
			applets.ColorForeground,
			dateStr,
			mpvStr,
			volStr,
			clockStr,
			powerStr)
	}
}
