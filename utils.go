package main

import (
	"time"
)

// Return channel for cancellation

func scroll(source, delim string, bannerWidth int, ch chan<- string, animTick time.Duration) chan struct{} {
	source = delim + source + delim
	for i := 0; i < 3; i++ {
		source = " " + source
	}

	ticker := time.NewTicker(animTick)

	done := make(chan struct{})

	go func() {
		for {
			select {
			case <-ticker.C:
				source = rotateString(source)
			case <-done:
				ticker.Stop()
				close(done)
				return
			}

			ch <- source[0:bannerWidth]
		}
	}()

	return done
}

func blink(source string, ch chan<- string, animTick time.Duration) {
	ticker := time.NewTicker(animTick)

	output := source

	for {
		select {
		case <-ticker.C:
			if output == blankString(source) {
				output = source
			} else {
				output = blankString(source)
			}
		}
		ch <- output
	}
}

func rotateString(str string) string {
	if len(str) == 0 {
		return ""
	}
	return str[1:] + str[:1]
}

func blankString(str string) (ret string) {
	for i := 0; i < len(str); i++ {
		ret += " "
	}

	return ret
}
