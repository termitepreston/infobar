package main

import (
	"time"
)

type applet interface {
	id() int
	write(time.Duration, chan<- Msg)
}

// clock applet

type clock struct {
	id int
}

func (c clock) write(delay time.Duration, ch chan<- Msg) {
	for {
		ch <- Msg{
			Clock,
			time.Now().Format("03:04:05 PM"),
		}
		time.Sleep(delay)
	}
}

func (c clock) id() int {
	return c.id
}

// end clock applet

type date struct {
	id int
}

func (d date) write(dt time.Duration, ch chan<- Msg) {
	for {
		ch <- Msg{
			Date,
			time.Now().Format("Mon Jan 02, 2006"),
		}

		time.Sleep(dt)
	}
}

func (d date) id() int {
	return d.id
}
